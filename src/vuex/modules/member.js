import * as myConst from '../../assets/js/const'
import jwtDecode from 'jwt-decode'
// Initial state 
const state = {
  loginState: false,
  access_token: ''
}
 
// Getter
const getters = {
  jwtDecode: (state) => jwtDecode(state.access_token)
}

const actions = {
  apilogin ({commit}, authentication) {
    $.ajax({
      url: myConst.API_URL + '/users/login',
      traditional: true,
      method: 'POST',
      data: authentication.form,
      dataType: 'json',
      success: function (result) {
        //console.log('d',result)
        authentication.callback(false, result)
      },
      error: function (result) {
        // console.log('e',result)
        authentication.callback(true, result)
        commit('UPDATE_STATUS', false)
      }
    })
  },
  updateStatus ({commit}, token){
    commit('UPDATE_STATUS', token)
  },
  checkLogin ({commit}, methods) {
    if (methods.cookie_pull('access_token')) {
      commit('UPDATE_STATUS', methods.cookie_pull('access_token'))
    } else {
      methods.cookie_clear()
      commit('UPDATE_STATUS', null)
    }
  }
}

const mutations = {
  UPDATE_STATUS (state, token){
    if(token) {
      state.access_token = token
      state.loginState = true
    }else{
      state.loginState = false
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}