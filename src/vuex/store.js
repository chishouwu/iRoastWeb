import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import member from './modules/member'
// import admin from './modules/admin'
// import faq from './modules/faq'
// import product from './modules/product'
// import news from './modules/news'

// Initialize
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
      member
  }
})
