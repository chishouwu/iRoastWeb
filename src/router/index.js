import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Login from '@/views/Login'
import Members from '@/views/Members'
import MemberEdit from '@/views/MemberEdit'
import MemberAdd from '@/views/MemberAdd'
import MemberDetail from '@/views/MemberDetail'
import Commodities from '@/views/Commodities'
import CommoditiesAdd from '@/views/CommoditiesAdd'
import CommoditiesEdit from '@/views/CommoditiesEdit'
import Curves from '@/views/Curves'
import CurvesRead from '@/views/CurvesRead'
import CurvesAdd from '@/views/CurvesAdd'
import CurvesEdit from '@/views/CurvesEdit'
import Beans from '@/views/Beans'
import BeanAdd from '@/views/BeanAdd'
import BeanEdit from '@/views/BeanEdit'
import Records from '@/views/Records'
import RecordsRead from '@/views/RecordsRead'
import Orders from '@/views/Orders'
import OrderAdd from '@/views/OrderAdd'
import Serials from '@/views/Serials'
import SerialAdd from '@/views/SerialAdd'
import Contracts from '@/views/Contracts'
import ContractAdd from '@/views/ContractAdd'
import ContractEdit from '@/views/ContractEdit'
import MemberCategory from '@/views/MemberCategory'
import Record from '@/views/Record'
import MemberRecord from '@/views/MemberRecord'
Vue.use(Router)

export default new Router({
  routes: [
    {path: '/home',name: 'Home',component: Home,meta: {adminOnly:true}},
    {path: '/login',name: 'Login',component: Login},
    {path: '/members',name: 'Members',component: Members, meta: { adminOnly: true }},
    {path: '/memberedit/:username', name: 'membereditor', component: MemberEdit, meta: { adminOnly: true } },
    {path: '/memberadd', name: 'memberadd', component: MemberAdd, meta: { adminOnly: true } },
    {path: '/memberdetail/:username', name: 'memberdetail', component: MemberDetail, meta: { adminOnly: true } },
    {path: '/commodities', name: 'Commodities', component: Commodities, meta: { adminOnly: true}},
    {path: '/Commoditiesadd', name: 'CommoditiesAdd', component: CommoditiesAdd, meta: { adminOnly: true}},
    {path: '/Commoditiesedit/:title', name: 'CommoditiesEdit', component: CommoditiesEdit, meta: { adminOnly: true}},
    {path: '/curves', name: 'Curves', component: Curves, meta: { adminOnly: true}},
    {path: '/curveadd', name: 'Curveadd', component: CurvesAdd, meta: { adminOnly: true}},
    {path: '/curve/:id', name: 'CurvesRead', component: CurvesRead, meta: { adminOnly: true}},
    {path: '/curveedit/:id', name: 'CurvesEdit', component: CurvesEdit, meta: { adminOnly: true}},
    {path: '/beans', name: 'Beans', component: Beans, meta: { adminOnly: true}},
    {path: '/beanadd', name: 'BeanAdd', component: BeanAdd, meta: { adminOnly: true}},
    {path: '/beanedit/:id', name: 'BeanEdit', component: BeanEdit, meta: { adminOnly: true}},
    {path: '/records', name: 'Records', component: Records, meta: { adminOnly: true}},
    {path: '/records/:id', name: 'RecordsRead', component: RecordsRead, meta: { adminOnly: true}},
    {path: '/orders', name: 'Orders', component: Orders, meta: { adminOnly: true}},
    {path: '/orderadd', name: 'OrderAdd', component: OrderAdd, meta: { adminOnly: true}},
    {path: '/serials', name: 'Serials', component: Serials, meta: { adminOnly: true}},
    {path: '/serialadd', name: 'SerialAdd', component: SerialAdd, meta: { adminOnly: true}},
    {path: '/contracts', name: 'Contracts', component: Contracts, meta: { adminOnly: true}},
    {path: '/contractadd', name: 'ContractAdd', component: ContractAdd, meta: { adminOnly: true}},
    {path: '/contract/:username', name: 'ContractEdit', component: ContractEdit, meta: { adminOnly: true}},
    {path: '/membercategory/:username', name: 'MemberCategory', component: MemberCategory, meta: { adminOnly: true}},
    {path: '/record/:username', name: 'Record', component: Record, meta: { adminOnly: true}},
    {path: '/record/:id', name: 'MemberRecord', component: MemberRecord, meta: { adminOnly: true}},
  ]
})
