import axios from 'axios'
import * as MY_CONST from './js/const'
import { API_URL } from './js/const';
export default {
    login: function({account,password}){
        return axios({
            method:'post',
            url: MY_CONST.API_URL+'/users/login',
            data:{
                username:account,
                password:password
            }
        })
        .then(function(res){
            return res
        }).catch(function(err){
            return err.response
        })
    }
};