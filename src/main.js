// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './components/App'
import store from './vuex/store'
import router from './router'
import $ from 'jquery'
// import 'bootstrap'
// import 'bootstrap/dist/css/bootstrap.css'
import axios from 'axios'
import Cookies from 'js-cookie'
import VeeValidate, { Validator } from 'vee-validate'
import messagesTw from './assets/js/lib/zh_CN' 

Vue.use(VeeValidate, {
  locale: 'cn',
  dictionary: {
    cn: {
      messages: messagesTw
    }
  }
})
Vue.config.productionTip = false
import VueSweetAlert from 'vue-sweetalert'

Vue.use(VueSweetAlert)
/* eslint-disable no-new */
router.beforeEach((to,from,next) => {
  if(to.meta.adminOnly && !Cookies.get('access_token')){
    next({ path: '/login' })
  }
  next()
})

new Vue({
  el: '#app',
  router: router,
  store: store,
  render: (h) => h(App),
})
