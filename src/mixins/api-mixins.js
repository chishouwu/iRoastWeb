// This mixin provides api related methods to any component
import * as myConst from '../assets/js/const'
export const apiMixins = {
  methods: {
    apiGetMembers(callback, token) {
      this.getAPI(callback, `/users`, 'GET', token)
    },
    apiGetMember(username, callback, token) {
      this.getAPI(callback, `/users/${username}`, 'GET', token)
    },
    apiPutMember(username, input, callback, token) {
      this.postAPI(input, callback, `/users/${username}`, 'PATCH', token)
    },
    apiPostMember(input, callback, token) {
      this.postAPI(input, callback, `/users`, 'POST', token)
    },
    apiDelUser(username, callback, token) {
      this.getAPI(callback, `/users/${username}`, 'DELETE', token)
    },
    apiGetCommodities(callback, token){
      this.getAPI(callback, '/commodities', 'GET', token)
    },
    apiGetCommodity(title, callback, token) {
      this.getAPI(callback, `/commodities/${title}`, 'GET', token)
    },
    apiPostCommodities(input, callback, token) {
      this.postAPI(input, callback, `/commodities`, 'POST', token)
    },
    apiDelCommodities(id, callback, token){
      this.getAPI(callback, `/commodities/${id}`, 'DELETE', token)
    },
    apiPutCommodities(title, input, callback, token) {
      this.postAPI(input, callback, `/commodities/${title}`, 'PATCH', token)
    },
    apiGetCurves(callback,token){
      this.getAPI(callback, '/curves', 'GET', token)
    },
    apiGetCurve(id,callback,token){
      this.getAPI(callback, `/curves/${id}`, 'GET', token)
    },
    apiPostCurve(input,callback,token){
      this.postAPI(input,callback, `/curves`, 'POST', token)
    },
    apiDelCurve(id, callback, token){
      this.getAPI(callback, `/curves/${id}`, 'DELETE', token)
    },
    apiGetCurve(id, callback, token) {
      this.getAPI(callback, `/curves/${id}`, 'GET', token)
    },
    apiPutCurve(id, input, callback, token) {
      this.postAPI(input, callback, `/curves/${id}`, 'PATCH', token)
    },
    apiGetBean(callback, token) {
      this.getAPI(callback, '/beans', 'GET', token)
    },
    apiPostBean(input,callback,token){
      this.postAPI(input,callback, '/beans', 'POST', token)
    },
    apiGetBeans(id, callback, token) {
      this.getAPI(callback, `/beans/${id}`, 'GET', token)
    },
    apiDelBean(id,callback,token){
      this.getAPI(callback,`/beans/${id}`,'DELETE',token)
    },
    apiPutBean(id,input,callback,token){
      this.postAPI(input, callback, `/beans/${id}`, 'PATCH', token)
    },
    apiGetRecord(id,callback,token){
      this.getAPI(callback,`/records/${id}`,'GET',token)
    },
    apiGetUserRecord(username,callback,token){
      this.getAPI(callback,`/records/user/${username}`,'GET',token)
    },
    apiGetRecords(callback,token){
      this.getAPI(callback,`/records`,'GET',token);
    },
    apiGetOrders(callback, token){
      this.getAPI(callback, '/orders', 'GET', token);
    },
    apiPostOrder(input,callback,token){
      this.postAPI(input, callback, '/orders', 'POST', token)
    },
    apiDelOrder(id,callback,token){
      this.getAPI(callback, `/orders/${id}`,'DELETE', token)
    },
    apiGetSerials(callback,token){
      this.getAPI(callback,`/serials`,'GET',token);
    },
    apiPostSerial(input,callback,token){
      this.postAPI(input, callback, '/serials', 'POST', token)
    },
    apiDelSerial(id,callback,token){
      this.getAPI(callback,`/serials/${id}`,'DELETE',token)
    },
    apiGetContracts(callback,token){
      this.getAPI(callback, `/contracts`, 'GET', token)
    },
    apiPostContract(input, callback, token) {
      this.postAPI(input, callback, `/contracts`, 'POST', token)
    },
    apiDelContract(id,callback,token){
      this.getAPI(callback,`/contracts/${id}`,'DELETE',token)
    },
    apiGetContract(username, callback, token) {
      this.getAPI(callback, `/contracts/${username}`, 'GET', token)
    },
    apiPutContract(username, input, callback, token) {
      this.postAPI(input, callback, `/contracts/${username}`, 'PATCH', token)
    },
    // --------------------------------
    // getAPI
    // --------------------------------
    getAPI(callback, APIName, httpMethod, token) {
      $.ajax({
        url: myConst.API_URL + APIName,
        method: httpMethod,
        contentType: 'application/json',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('x-access-token', token)
        },
        success: function (result) {
          console.log('成功請求結果', result)
          callback(false, result)
        },
        error: function (result) {
          console.log('失敗請求結果', result)
          callback(true, result)
        }
      })
    },
    
    // --------------------------------
    // postAPI
    // --------------------------------
    postAPI(input, callback, APIName, httpMethod, token) {
      $.ajax({
        url: myConst.API_URL + APIName,
        traditional: true,
        method: httpMethod,
        data: input,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('x-access-token', token)
        },
        success: function (result) {
          console.log('成功請求結果', result)
          callback(false, result)
        },
        error: function (result) {
          console.log('失敗請求結果', result)
          callback(true, result)
        }
      })
    }
  }
}