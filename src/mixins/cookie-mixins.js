import Cookies from 'js-cookie'
export const cookieMixin = {
  methods: {
    cookie_push (key, value, expire) {
      return Cookies.set(`${key}`, value, {expires: expire})
    },

    // Here we read a message off the browser's local storage.
    cookie_pull (key) {
      return Cookies.get(`${key}`)
    },

    cookie_clear () {
      return Object.keys(Cookies.get()).forEach((cookie) => {
        Cookies.remove(cookie)
      })
    }
  }
}